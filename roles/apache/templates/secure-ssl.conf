{%
set newer_distro = (
    (ansible_distribution == 'Ubuntu' and ansible_distribution_version is version('18.04', '>=')) or
    (ansible_distribution == 'Debian' and ansible_distribution_version is version('10', '>='))
)
%}
{% if newer_distro and not use_old_apache_config|default(False) %}
{# This is a newer distro version where Apache supports http2 and uses libssl 1.1.1 #}
SSLEngine               off
SSLProtocol             all -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
SSLHonorCipherOrder     on
SSLCompression          off
SSLSessionTickets       off

# Ciphers we're allowed to use
SSLCipherSuite ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256
{% else %}
SSLEngine               off
SSLProtocol             all -SSLv2 -SSLv3
SSLHonorCipherOrder     on
SSLCompression          off

# Ciphers we're allowed to use
SSLCipherSuite ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-DSS-AES128-GCM-SHA256:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-DSS-AES128-SHA256:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:DHE-RSA-AES256-SHA:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK
{% endif %}

# OCSP configuration
SSLUseStapling          on
SSLStaplingResponderTimeout 5
SSLStaplingFakeTryLater off
SSLStaplingReturnResponderErrors off
SSLStaplingCache        shmcb:/var/run/ocsp(128000)

{% if apache_header_table == 'onsuccess' %}
    {%- set header_set = "Header set" %}
    {%- set header_unset = "Header always unset" %}
{% elif apache_header_table == 'always' %}
    {%- set header_set = "Header always set" %}
    {%- set header_unset = "Header onsuccess unset" %}
{% endif -%}

# Use HSTS and other relevant headers
{{header_set}} Strict-Transport-Security "max-age=15768000"
{{header_set}}ifempty Referrer-Policy "strict-origin-when-cross-origin"
{{header_set}}ifempty X-XSS-Protection "1; mode=block"
{{header_set}}ifempty X-Content-Type-Options "nosniff"
{{header_set}}ifempty X-Frame-Options "sameorigin"

# Make sure anything in the 'onsuccess' table is moved out of the way for the above headers
# This works around an Apache oddity in the way it handles setting headers
# The only module known to use the 'onsuccess' table is proxy_http, so in practice this has minimal risk for us
# proxy_fcgi (for PHP) and regular CGI scripts use the always table and will be covered by the above
{{header_unset}} Strict-Transport-Security
{{header_unset}} Referrer-Policy
{{header_unset}} X-XSS-Protection
{{header_unset}} X-Content-Type-Options
{{header_unset}} X-Frame-Options
{% if newer_distro and not use_old_apache_config|default(False) %}

# Permit HTTP/2
Protocols h2 http/1.1
{% endif %}
