- name: install OpenLDAP
  apt:
    package: slapd
    state: present

- name: install our /etc/default/slapd
  template:
    src: slapd-default
    dest: /etc/default/slapd
    owner: root
    group: root
    mode: 0644
  notify: restart OpenLDAP

- name: create schema directory
  file:
    path: /etc/ldap/kde-schema
    state: directory
    owner: root
    group: root
    mode: 0755

- name: install KDE LDAP schemas
  copy:
    remote_src: yes
    src: /srv/www/identity.kde.org/protected/data/{{item}}
    dest: /etc/ldap/kde-schema/{{item}}
    mode: 0644
  with_items:
    - kde.schema
    - openssh.schema
    - rfc2307bis.schema
  notify: restart OpenLDAP

- name: install our slapd.conf
  template:
    src: slapd.conf
    dest: /etc/ldap/slapd.conf
    owner: root
    group: openldap
    mode: 0640
  notify: restart OpenLDAP

# If we just installed the config files above,
# we need OpenLDAP to restart *now*,
# so that later tasks can connect to ldap
- name: restart LDAP if needed
  meta: flush_handlers

- name: install python-ldap
  apt:
    package: python-ldap
    state: present

# This will only create LDAP entries if they don't exist.
# Changes to directory.ldif will not apply to existing LDAP entries!
- name: create initial LDAP entries
  ldap_entry:
    server_uri: ldap://localhost
    bind_dn: cn=Sysadmin,dc=kde,dc=org
    bind_pw: "{{ldap_root_password}}"
    dn: "{{item.dn}}"
    attributes: "{{item.attrs}}"
    objectClass: "{{item.objectClass}}"

  loop: "{{lookup('template', 'directory.ldif') | parse_ldif}}"
  loop_control:
    label: "{{item.dn}}"

- name: create ldapconnect user
  user:
    name: ldapconnect
    password_lock: yes

- name: ensure ldapconnect/.ssh exists
  file:
    path: /home/ldapconnect/.ssh
    state: directory
    owner: root
    group: root
