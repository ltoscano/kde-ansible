LOCATION=/root/{{backup_directory}}

# Cleanup old backups...
find $LOCATION -name '*gz' -mtime +7 | xargs rm -f

# Grab software we've installed
tar -czf $LOCATION/srv-files-bluemchen.`date +%w`.tgz -C / srv/

# Grab the system config, package listing and crontabs
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
tar -czf $LOCATION/root-bin-bluemchen.`date +%w`.tgz -C / root/bin/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Secure the backups
chmod -R 700 $LOCATION

# Transfer them to the backup server
cd $LOCATION/..
{% set backup_creds = hetzner_backup_creds[hetzner_backup_host] %}
rsync --timeout=600 --delete -a -e 'ssh -p23' {{backup_directory}}/ {{backup_creds.username}}@{{backup_creds.hostname}}:backups/

# Also backup home directories. As they're quite large we just transfer them directly
rsync --timeout=600 --delete -a -e 'ssh -p23' --no-devices --no-specials /home/ {{backup_creds.username}}@{{backup_creds.hostname}}:homes/ | grep -v 'skipping non-regular file'
