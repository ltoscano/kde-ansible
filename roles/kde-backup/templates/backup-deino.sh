LOCATION=/root/backups

# Backup mirrors listing
~mirroradmin/bin/mirrorbits-download list -http -ftp -rsync > $LOCATION/mirrors-download.`date +%w`
~mirroradmin/bin/mirrorbits-files list -http -ftp -rsync > $LOCATION/mirrors-files.`date +%w`

# Backup configuration and list of installed packages
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
dpkg -l > $LOCATION/dpkg.`date +%w`

# Transfer traditional backups to Hetzner Backup Space
lftp -f ~/bin/backup-options

# Prepare to run Borg backups
export BORG_PASSPHRASE='{{backup_borg_passphrase}}'

{% set backup_creds = hetzner_backup_creds[hetzner_backup_host] %}
BORG_SERVER="ssh://{{backup_creds.username}}@{{backup_creds.hostname}}:23"

# Backup Homes
export BORG_REPO="$BORG_SERVER/./borg-backups/homes"
borg create --compression none --exclude-caches ::'{hostname}-homes-{now}' /home/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"

# Backup Distribute
export BORG_REPO="$BORG_SERVER/./borg-backups/cdn"
borg create --compression none --exclude-caches ::'{hostname}-cdn-{now}' /srv/www/cdn.kde.org/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"

# Backup Distribute
export BORG_REPO="$BORG_SERVER/./borg-backups/distribute"
borg create --compression none --exclude-caches ::'{hostname}-distribute-{now}' /srv/www/distribute.kde.org/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 3 2>&1 | grep -v "Compacting segments"

# Backup Download
export BORG_REPO="$BORG_SERVER/./borg-backups/download"
borg create --compression none --exclude-caches ::'{hostname}-download-{now}' /srv/archives/ftp/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"

# Backup Files
export BORG_REPO="$BORG_SERVER/./borg-backups/files"
borg create --compression none --exclude-caches ::'{hostname}-files-{now}' /srv/archives/files/ 2>&1 | grep -v "Compacting segments"
borg prune --prefix '{hostname}-' --keep-daily 7 --keep-weekly 4 --keep-monthly 6 2>&1 | grep -v "Compacting segments"
