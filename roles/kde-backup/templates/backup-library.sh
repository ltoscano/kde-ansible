LOCATION=/root/{{backup_directory}}

# Cleanup old backups
find $LOCATION/ -name '*gz' -mtime +7 | xargs rm -f

# Grab the system configuration, package listing and cronjobs
dpkg -l > $LOCATION/dpkg.`date +%w`
tar -czf $LOCATION/crontabs.`date +%w`.tgz -C / var/spool/cron/
tar -czf $LOCATION/etc.`date +%w`.tgz -C / etc/

# Secure our backups
chmod -R 700 $LOCATION

# Transfer them to the backup server now
cd $LOCATION/..
{% set backup_creds = hetzner_backup_creds[hetzner_backup_host] %}
rsync --timeout=600 --delete -a -e 'ssh -p23' {{backup_directory}}/ {{backup_creds.username}}@{{backup_creds.hostname}}:backups/
