---

- name: install packages
  apt:
    name:
      - php5.6-fpm
      - php5.6-ldap
      - php5.6-mcrypt
      - php5.6-mysql
      - php-pear
      - php-imagick

- name: create identity user
  user:
    name: identity
    state: present

- name: ensure www directory exists
  file:
    path: /srv/www
    state: directory

# In the normal case, we clone the git repository to get the code.

- name: setup docroot (git)
  when: not (vagrant_local_code | default(False) | bool)
  block:

  - name: ensure www/identity directory exists
    file:
      path: /srv/www/identity.kde.org
      state: directory
      owner: identity
      group: identity

  - name: get Identity code from Git
    become: yes
    become_user: identity
    git:
      dest: /srv/www/identity.kde.org
      repo: https://invent.kde.org/websites/identity-kde-org

  - name: create runtime directories
    file:
      state: directory
      dest: /srv/www/identity.kde.org/{{item}}
      owner: identity
      group: www-data
      mode: 0775
    with_items:
      - assets
      - protected/runtime
  # end block

# If we're using the vagrant environment for website dev,
# we get the code from the /vagrant shared folder instead.

- name: setup docroot (vagrant)
  when: (vagrant_local_code | default(False) | bool)
  block:

  - name: symlink Identity code from /vagrant
    file:
      state: link
      src: /vagrant
      dest: /srv/www/identity.kde.org

  # The 'assets' and 'runtime' directories need GID www-data
  # so that they are writable by Apache,
  # but we can't change permissions of stuff in /vagrant
  # because shared folders don't support it.
  # Instead, put them elsewhere and use symlinks.
  # It's probably better to keep runtime data in the VM
  # and not in the host anyway.
  - name: create assets directory
    file:
      state: directory
      dest: /srv/www/identity-assets
      group: www-data
      mode: 0775

  - name: symlink assets directory
    file:
      state: link
      src: /srv/www/identity-assets
      dest: /srv/www/identity.kde.org/assets

  - name: create runtime directory
    file:
      state: directory
      dest: /srv/www/identity-runtime
      group: www-data
      mode: 0775

  - name: symlink runtime directory
    file:
      state: link
      src: /srv/www/identity-runtime
      dest: /srv/www/identity.kde.org/protected/runtime
  # end block

- name: install Solena config
  template:
    src: main.php
    dest: /srv/www/identity.kde.org/protected/config/main.php
    # if we're in vagrant, don't bother touching ownership;
    # it won't work on shared folders and will spuriously
    # report a change on every run
    owner: "{{vagrant_local_code | default(False) | ternary(omit, 'identity')}}"
    group: "{{vagrant_local_code | default(False) | ternary(omit, 'identity')}}"

- name: install virtualhost config
  template:
    src: identity-apache-vhost.conf
    dest: /etc/apache2/sites-available/identity.kde.org.conf
    owner: root
    group: root
    mode: 0644
  notify: reload Apache
  tags: vhost-config

- name: enable virtualhost config
  file:
    state: link
    src: ../sites-available/identity.kde.org.conf
    dest: /etc/apache2/sites-enabled/identity.kde.org.conf
  notify: reload Apache

# This task runs as mysql root (the identity user doesn't have 'create database' privileges).
# If the database doesn't exist, it will be created, and the task will report 'changed';
# then the next task will import the schema. On next run, the database will already exist,
# so this task will do nothing, and we use that to skip the schema import task.
- name: create identity database
  mysql_db:
    name: identity
    state: present
  register: identity_db_create

- name: import database schema
  mysql_db:
    name: identity
    state: import
    login_user: identity
    login_password: "{{ mysql_user_password }}"
    target: /srv/www/identity.kde.org/protected/data/database-schema.sql
  when: identity_db_create.changed
